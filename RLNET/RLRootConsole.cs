﻿#region license
/* 
 * Released under the MIT License (MIT)
 * Copyright (c) 2014 Travis M. Clark
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
#endregion

using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;

namespace RLNET
{
    public class RLRootConsole : RLGUIConsole
    {
        //TODO: make cell array as big as player can zoomout and just use camera zoom to determine what draw
        private RLDynamicObject dynamicScreen;
        private int dynamicScreenWidth;
        private int dynamicScreenHeight;

        public RLRootConsole(string bitmapFile, int width, int height, int charWidth, int charHeight, string guiVertexShader, string guiFragmentShader, float scale = 1, string title = "RLNET Console")
            : base(bitmapFile, width, height, charWidth, charHeight, guiVertexShader,guiFragmentShader,scale, title)
        {
        }

        public RLRootConsole(RLSettings settings, string guiVertexShader, string guiFragmentShader)
            : base(settings,guiVertexShader,guiFragmentShader)
        {

        }

        public void InitGameScreen(string vertexShader, string fragmentShader, bool useBackColor, float maxZoomOut)
        {
            this.dynamicScreenWidth = (int) (this.Width*maxZoomOut);
            this.dynamicScreenHeight = (int) (this.Height*maxZoomOut);
            dynamicScreen = new RLDynamicObject(dynamicScreenWidth, dynamicScreenHeight);
            dynamicScreen.UseMapBuffer = true;
            dynamicScreen.UseBackColor = useBackColor;
            
            // dynamicScreen.CreateShader(vertexShader,fragmentShader);
            //dynamicScreen.shader.UseShader = false;
            CreateDynamicObjectBuffers(dynamicScreen);

        }

        public void SetGameScreenBuffer(int x, int y,  RLCell cell)
        {
            if ((x < 0 || x >= dynamicScreenWidth || y < 0 || y >= dynamicScreenHeight))
                return;
            dynamicScreen.FrontColorChanged = true;

            dynamicScreen.renderedCells[x,y] = cell;
        }
        public void SetGameScreenBuffer(float x, float y, RLCell cell)
        {
            SetGameScreenBuffer((int)x, (int)y, cell);

        }
        public void SetGameScreenBuffer(double x, double y, RLCell cell)
        {
            SetGameScreenBuffer((int)x, (int)y, cell);

        }

        public void RemoveLastShaderArgumentFromScreenBuffer()
        {

            int c = this.dynamicScreen.shader.PassArgumentMethods.Count;
            if(c>1)
                this.dynamicScreen.shader.PassArgumentMethods.RemoveAt(c - 1);
        }
        //TODO: move screenbuffer logic to class
        public void AddShaderArgumentToGameScreenBuffer(Action<RLShader> method)
        {
            this.dynamicScreen.shader.PassArgumentMethods.Add(method);
        }
        public void ClearGameScreenBuffer()
        {
            for (int iy = 0; iy < dynamicScreenHeight; iy++)
            {
                for (int ix = 0; ix < dynamicScreenWidth; ix++)
                {
                    dynamicScreen.renderedCells[ix, iy].backColor = RLColor.Black;
                    dynamicScreen.renderedCells[ix, iy].color = RLColor.White;
                    dynamicScreen.renderedCells[ix, iy].character = -1;
                    //zBuffer[ix, iy] = int.MinValue;
                }

            }
            dynamicScreen.ModelChanged = true;
        }
        public void ClearGameScreenBuffer(RLCell cell)
        {
            for (int iy = 0; iy < dynamicScreenHeight; iy++)
            {
                for (int ix = 0; ix < dynamicScreenWidth; ix++)
                {
                    dynamicScreen.renderedCells[ix, iy] = cell;
                    //zBuffer[ix, iy] = int.MinValue;
                }

            }
            dynamicScreen.ModelChanged = true;
        }
        public void DrawGameScreenBuffer(RLColor transparentColor)
        {
            DrawDynamicObject(transparentColor,dynamicScreen, Vector2.Zero, 0, Vector2.Zero);
        }



    }

}
