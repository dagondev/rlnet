﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace RLNET
{
    public class RLDynamicObject : RLObject
    {
        public RLCell[,] renderedCells;
        /// <summary>
        /// Copy of created model for reference as Model itself maybe heavily changed.
        /// </summary>
        public RLCell[,] unalteredCells;
        public RLCell[,] rotatedCells;
        public bool ModelChanged;
        public bool FrontColorChanged;
        public bool UseBackColor;
        /// <summary>
        /// Use this only when you know colors will change *every* frame
        /// </summary>
        public bool UseMapBuffer;
        internal RLShader shader;
/*        public RLDynamicObject(RLDynamicObject model)
        {
            renderedCells = new RLCell[model.renderedCells.GetLength(0), model.renderedCells.GetLength(1)];
            for (int x = 0; x < model.renderedCells.GetLength(0); x++)
            {
                for (int y = 0; y < model.renderedCells.GetLength(1); y++)
                {
                    renderedCells[x,y]=new RLCell(model.renderedCells[x,y]);
                }
            }
            ModelChanged = true;
            UseBackColor = model.UseBackColor;
            UseMapBuffer = model.UseMapBuffer;
            FrontColorChanged = model.FrontColorChanged;
            this.vboId = model.vboId;
            this.backColorId = model.backColorId;
            this.backColorVertices = model.backColorVertices;
            this.colorVertices = model.colorVertices;
            this.foreColorId = model.foreColorId;
            this.iboId = model.iboId;
            this.tcboId = model.tcboId;
            this.texVertices = model.texVertices;
            
            shader = model.shader;
        }*/
        public RLDynamicObject(int width, int height)
        {
            renderedCells = new RLCell[width, height];
            int len = width > height ? width : height;
            ModelChanged = true;
            UseBackColor = true;
            UseMapBuffer = false;
        }

        public void CreateUnalteredModel()
        {
            unalteredCells = new RLCell[renderedCells.GetLength(0), renderedCells.GetLength(1)];
            for (int x = 0; x < renderedCells.GetLength(0); x++)
            {
                for (int y = 0; y < renderedCells.GetLength(1); y++)
                {
                    unalteredCells[x, y] = new RLCell(renderedCells[x, y]);
                }
            }
        }
        public void CreateShader(string vertexShaderText, string fragmentShaderText)
        {
            shader = new RLShader(vertexShaderText, fragmentShaderText);

        }

    }



}
