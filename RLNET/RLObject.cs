﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace RLNET
{
    public class RLObject
    {

        internal int vboId; //position bo
        internal int iboId; //index bo
        internal int tcboId; //tex coord bo
        internal int foreColorId; //color3 bo
        internal int backColorId; //color3 bo
        internal Vector2[] texVertices;
        internal Vector3[] colorVertices;
        internal Vector3[] backColorVertices;


    }
}
