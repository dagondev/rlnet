﻿#region license
/* 
 * Released under the MIT License (MIT)
 * Copyright (c) 2014 Travis M. Clark
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
#endregion

using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;


namespace RLNET
{
    public class RLGUIConsole : RLConsole
    {
        private GameWindow window;
        private bool closed = false;

        protected float uRatio;
        protected float vRatio;
        protected int charsPerRow;
        public double DeltaInSeconds { get; private set; }
        public double TotalTime { get; private set; }

        private float scale;
        private Vector3 scale3;
        private RLResizeType resizeType;
        private int offsetX;
        private int offsetY;

        public event UpdateEventHandler Render;
        public event UpdateEventHandler Update;
        public event EventHandler<EventArgs> OnLoad;
        public event EventHandler<System.ComponentModel.CancelEventArgs> OnClosing;
        public event ResizeEventHandler OnResize;

        public RLKeyboard Keyboard { get; private set; }
        public RLMouse Mouse { get; private set; }

        public int CharWidth;
        public int CharHeight;

        /// <summary>
        /// Creates the root console.
        /// </summary>
        /// <param name="bitmapFile">Path to bitmap file.</param>
        /// <param name="width">Width in characters of the console.</param>
        /// <param name="height">Height in characters of the console.</param>
        /// <param name="charWidth">Width of the characters.</param>
        /// <param name="charHeight">Height of the characters.</param>
        /// <param name="scale">Scale the window by this value.</param>
        /// <param name="title">Title of the window.</param>
        public RLGUIConsole(string bitmapFile, int width, int height, int charWidth, int charHeight, string guiVertexShader, string guiFragmentShader, float scale = 1f,
            string title = "RLNET Console")
            : base(width/charWidth, height/charHeight)
        {
            RLSettings settings = new RLSettings();
            settings.BitmapFile = bitmapFile;
            //settings.BitmapAlphaFile = bitmapAlphaFile;
            settings.Width = width/charWidth;
            settings.Height = height/charHeight;
            settings.CharWidth = charWidth;
            settings.CharHeight = charHeight;
            settings.Scale = scale;
            settings.Title = title;

            Init(settings,guiVertexShader,guiFragmentShader);

        }


        /// <summary>
        /// Creates the root console.
        /// </summary>
        /// <param name="settings">Settings for the RLRootConsole.</param>
        public RLGUIConsole(RLSettings settings, string guiVertexShader, string guiFragmentShader)
            : base(settings.Width, settings.Height)
        {
            Init(settings, guiVertexShader, guiFragmentShader);

        }

        internal virtual void Init(RLSettings settings, string guiVertexShader, string guiFragmentShader)
        {


            if (settings == null)
                throw new ArgumentNullException("settings");
            if (settings.BitmapFile == null)
                throw new ArgumentNullException("BitmapFile");
            //if (settings.BitmapAlphaFile == null)
            //    throw new ArgumentNullException("BitmapAlphaFile");
            if (settings.Title == null)
                throw new ArgumentNullException("Title");
            if (settings.Width <= 0)
                throw new ArgumentException("Width cannot be zero or less");
            if (settings.Height <= 0)
                throw new ArgumentException("Height cannot be zero or less");
            if (settings.CharWidth <= 0)
                throw new ArgumentException("CharWidth cannot be zero or less");
            if (settings.CharHeight <= 0)
                throw new ArgumentException("CharHeight cannot be zero or less");
            if (settings.Scale <= 0)
                throw new ArgumentException("Scale cannot be zero or less");
            if (System.IO.File.Exists(settings.BitmapFile) == false)
                throw new System.IO.FileNotFoundException("cannot find bitmapFile");



            this.scale = settings.Scale;
            this.scale3 = new Vector3(scale, scale, 1f);
            this.CharWidth = settings.CharWidth;
            this.CharHeight = settings.CharHeight;
            this.resizeType = settings.ResizeType;

            //window = new GameWindow((int)(settings.Width * CharWidth * scale), (int)(settings.Height * CharHeight * scale), GraphicsMode.Default,"Game Window",GameWindowFlags.Default,DisplayDevice.Default,2,1,GraphicsContextFlags.AngleD3D9);
            //window = new GameWindow();
            GraphicsMode mode = new GraphicsMode(GraphicsMode.Default.ColorFormat,0,0,0);

            window = new GameWindow((int) (settings.Width*CharWidth*scale), (int) (settings.Height*CharHeight*scale), mode);
            window.VSync=VSyncMode.On;


           window.WindowBorder = (WindowBorder)settings.WindowBorder;
            if (settings.StartWindowState == RLWindowState.Fullscreen || settings.StartWindowState == RLWindowState.Maximized)
            {
                window.WindowState = (WindowState)settings.StartWindowState;
            }

            window.Title = settings.Title;
            window.RenderFrame += window_RenderFrame;
            window.UpdateFrame += window_UpdateFrame;
            window.Load += window_Load;
            window.Resize += window_Resize;
            window.Closed += window_Closed;
            window.Closing += window_Closing;
            Mouse = new RLMouse(window);
            Keyboard = new RLKeyboard(window);

            //LoadFontTexture(settings.BitmapAlphaFile, true);       

            guiDynamicObject = new RLDynamicObject(settings.Width, settings.Height);

            guiDynamicObject.CreateShader(guiVertexShader, guiFragmentShader);
            guiObject = new RLGUIObject();

            CreateDynamicObjectBuffers(guiDynamicObject);

            LoadFontTexture(settings.BitmapFile, false);

            CalcWindow(true);

            var s = GL.GetString(StringName.Version);
            Console.WriteLine("OpenGL version: "+s);
        }

        void window_Load(object sender, EventArgs e)
        {
            window.VSync = VSyncMode.On;
            if (OnLoad != null) OnLoad(this, e);
        }

        void window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (OnClosing != null) OnClosing(this, e);
        }

        /// <summary>
        /// Resizes the window.
        /// </summary>
        /// <param name="width">The new width of the window, in pixels.</param>
        /// <param name="height">The new height of the window, in pixels.</param>
        public void ResizeWindow(int width, int height)
        {

            Resize(width / CharWidth, height / CharHeight);
            RLDynamicObject old = guiDynamicObject;
            guiDynamicObject = new RLDynamicObject(Width, Height);
            guiDynamicObject.CreateShader(old.shader.VertexShaderText, old.shader.FragmentShaderText);
            CreateDynamicObjectBuffers(guiDynamicObject);
            window.Width = width;
            window.Height = height;
            GL.Viewport(window.ClientRectangle);
        }

        public void SetWindowState(RLWindowState windowState)
        {
            window.WindowState = (WindowState)windowState;
        }

        public void LoadBitmap(string bitmapFile, int charWidth, int charHeight)
        {
            if (bitmapFile == null)
                throw new ArgumentNullException("bitmapFile");
            if (charWidth <= 0)
                throw new ArgumentException("charWidth cannot be zero or less");
            if (charHeight <= 0)
                throw new ArgumentException("charHeight cannot be zero or less");

            if (this.CharWidth != charWidth || this.CharHeight != charHeight)
            {
                this.CharWidth = charWidth;
                this.CharHeight = charHeight;

                if (resizeType == RLResizeType.ResizeCells || window.WindowState == WindowState.Fullscreen || window.WindowState == WindowState.Maximized)
                {
                    CalcWindow(false);
                }
                else
                {
                    int viewWidth = (int)(Width * charWidth * scale);
                    int viewHeight = (int)(Height * charHeight * scale);

                    if (viewWidth != window.Width || viewHeight != window.Height)
                    {
                        ResizeWindow(viewWidth, viewHeight);
                    }
                    else
                    {
                        CalcWindow(false);
                    }
                }
            }
            LoadFontTexture(bitmapFile,false);
        }

        public void Close()
        {
            window.Close();
        }

        void window_Resize(object sender, EventArgs e)
        {
            CalcWindow(false);
        }

        private void CalcWindow(bool startup)
        {
            if (resizeType == RLResizeType.None)
            {
                int viewWidth = (int)(Width * CharWidth * scale);
                int viewHeight = (int)(Height * CharHeight * scale);
                int newOffsetX = (window.Width - viewWidth) / 2;
                int newOffsetY = (window.Height - viewHeight) / 2;

                if (startup || offsetX != newOffsetX || offsetY != newOffsetY)
                {
                    offsetX = newOffsetX;
                    offsetY = newOffsetY;
                    Mouse.Calibrate(CharWidth, CharHeight, offsetX, offsetY, scale);
                    GL.Viewport(offsetX, offsetY, viewWidth, viewHeight);
                }


            }
            else if (resizeType == RLResizeType.ResizeCells)
            {
                int width = window.Width / CharWidth;
                int height = window.Height / CharHeight;

                if (startup || width != Width || height != Height)
                {
                    Resize(width, height);
                    //TODO: clean last guiDynamicObject
                    RLDynamicObject old = guiDynamicObject;
                    guiDynamicObject = new RLDynamicObject(Width, Height);

                    guiDynamicObject.CreateShader(old.shader.VertexShaderText, old.shader.FragmentShaderText);
                    CreateDynamicObjectBuffers(guiDynamicObject);
                    GL.Viewport(0, 0, (int)(Width * CharWidth * scale), (int)(Height * CharHeight * scale));
                    Mouse.Calibrate(CharWidth, CharHeight, offsetX, offsetY, scale);
                    if (OnResize != null) OnResize(this, new ResizeEventArgs(width, height));
                }
            }
            else if (resizeType == RLResizeType.ResizeScale)
            {
                float newScale = Math.Min(window.Width / (CharWidth * Width), window.Height / (CharHeight * Height));
                int viewWidth = (int)(Width * CharWidth * newScale);
                int viewHeight = (int)(Height * CharHeight * newScale);
                int newOffsetX = (window.Width - viewWidth) / 2;
                int newOffsetY = (window.Height - viewHeight) / 2;

                if (startup || newScale != scale || offsetX != newOffsetX || offsetY != newOffsetY)
                {
                    offsetX = newOffsetX;
                    offsetY = newOffsetY;
                    scale = newScale;
                    scale3 = new Vector3(scale, scale, 1);
                    Mouse.Calibrate(CharWidth, CharHeight, offsetX, offsetY, scale);
                    GL.Viewport(offsetX, offsetY, viewWidth, viewHeight);
                }

                if (startup)
                {
                    //TODO: clean last guiDynamicObject
                    RLDynamicObject old = guiDynamicObject;
                    guiDynamicObject = new RLDynamicObject(Width, Height);

                    guiDynamicObject.CreateShader(old.shader.VertexShaderText, old.shader.FragmentShaderText);
                    CreateDynamicObjectBuffers(guiDynamicObject);
                }
            }
        }

        ~RLGUIConsole()
        {
            if (window != null)
                window.Dispose();

            if (!closed)
            {/*
                GL.DeleteBuffer(guiVboId);
                GL.DeleteBuffer(guiIboId);
                GL.DeleteBuffer(guiTcboId);
                GL.DeleteBuffer(guiForeColorId);
                GL.DeleteBuffer(guiBackColorId);
                GL.DeleteTexture(guiTexId);*/
            }
        }


        /// <summary>
        /// Opens the window and begins the game loop.
        /// </summary>
        public void Run(double fps = 30d)
        {
            window.Run(fps);
        }

        /// <summary>
        /// Gets or sets the title of the window.
        /// </summary>
        public string Title
        {
            set
            {
                window.Title = value;
            }
            get
            {
                return window.Title;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the mouse cursor is visible.
        /// </summary>
        public bool CursorVisible
        {
            set
            {
                window.CursorVisible = value;
            }
            get
            {
                return window.CursorVisible;
            }
        }

        /// <summary>
        /// Returns whether the window has been closed.
        /// </summary>
        /// <returns></returns>
        public bool IsWindowClosed()
        {
            return closed;
        }

        private void window_UpdateFrame(object sender, FrameEventArgs e)
        {
            DeltaInSeconds = e.Time;
            Keyboard.PollKeyEvents(DeltaInSeconds);
            TotalTime += DeltaInSeconds;
            if (Update != null)
                Update(this, new UpdateEventArgs(e.Time));
        }

        private void window_RenderFrame(object sender, FrameEventArgs e)
        {
            if (Render != null)
                Render(this, new UpdateEventArgs(e.Time));
        }

        private void window_Closed(object sender, EventArgs e)
        {
            closed = true;
/*
            GL.DeleteBuffer(guiVboId);
            GL.DeleteBuffer(guiIboId);
            GL.DeleteBuffer(guiTcboId);
            GL.DeleteBuffer(guiForeColorId);
            GL.DeleteBuffer(guiBackColorId);
            GL.DeleteTexture(guiTexId);*/
            //TODO: clean dynamic objects
        }


        public void DrawGUI(RLColor transparentColor)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Width * CharWidth * scale, Height * CharHeight * scale, 0, -1, 1);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            DrawDynamicObject(transparentColor,guiDynamicObject,Vector2.Zero,0,Vector2.Zero);
        }

        public void StartDrawing(float cameraZoom)
        {
            //Clear
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(Color.Black);
            //Set Projection
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Width * CharWidth * scale*cameraZoom, Height * CharHeight * scale * cameraZoom, 0, -1, 1);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            //Setup States
            GL.Enable(EnableCap.VertexArray);
            GL.Enable(EnableCap.IndexArray);
            GL.Enable(EnableCap.ColorArray);
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.Lighting);
            GL.Disable(EnableCap.DepthTest);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, guiObject.guiTexId);

            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.IndexArray);
            GL.EnableClientState(ArrayCap.ColorArray);

            GL.Scale(scale3);
        }
        public void EndDrawing()
        {
            //Clean Up
            GL.Disable(EnableCap.Texture2D);
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.DisableClientState(ArrayCap.IndexArray);
            GL.DisableClientState(ArrayCap.ColorArray);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            window.SwapBuffers();
        }

        protected uint[] CreateIndices(int cellCount)
        {
            uint[] indices = new uint[cellCount * 6];

            for (uint i = 0; i < cellCount; i++)
            {
                uint iv = (uint)(i * 4);
                uint ii = (uint)(i * 6);
                indices[ii] = iv;
                indices[ii + 1] = (uint)(iv + 1);
                indices[ii + 2] = (uint)(iv + 2);
                indices[ii + 3] = (uint)(iv + 2);
                indices[ii + 4] = (uint)(iv + 3);
                indices[ii + 5] = iv;
            }

            return indices;
        }

        public void CreateDynamicObjectBuffers(List<RLDynamicObject> objs)
        {
            foreach (RLDynamicObject obj in objs)
            {
                CreateDynamicObjectBuffers(obj);
            }
        }

        public void CreateDynamicObjectBuffers(RLDynamicObject obj)
        {
            obj.vboId = GL.GenBuffer();
            obj.iboId = GL.GenBuffer();
            obj.tcboId = GL.GenBuffer();
            obj.foreColorId = GL.GenBuffer();
            obj.backColorId = GL.GenBuffer();

            int width = obj.renderedCells.GetLength(0);
            int height = obj.renderedCells.GetLength(1);

            Vector2[] vertices = CreateDynamicVertices(width, height, CharWidth, CharHeight);
            GL.BindBuffer(BufferTarget.ArrayBuffer, obj.vboId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * 2 * sizeof(float)), vertices,
                BufferUsageHint.StaticDraw);

            obj.texVertices = new Vector2[width * height * 4];
            GL.BindBuffer(BufferTarget.ArrayBuffer, obj.tcboId);
            if (obj.UseMapBuffer)
            {
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.texVertices.Length * 2 * sizeof(float)),
                    IntPtr.Zero,
                    BufferUsageHint.StreamDraw);
            }
            else
            {

                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.texVertices.Length * 2 * sizeof(float)),
                    obj.texVertices,
                    BufferUsageHint.StreamDraw);
            }
            obj.colorVertices = new Vector3[width * height * 4];
            GL.BindBuffer(BufferTarget.ArrayBuffer, obj.foreColorId);
            if (obj.UseMapBuffer)
            {
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.colorVertices.Length * 3 * sizeof(float)), IntPtr.Zero,
 BufferUsageHint.StreamDraw);
            }
            else
            {

                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.colorVertices.Length * 3 * sizeof(float)),
                    obj.colorVertices, BufferUsageHint.StreamDraw);
            }
            if (obj.UseBackColor)
            {
                obj.backColorVertices = new Vector3[width * height * 4];

                GL.BindBuffer(BufferTarget.ArrayBuffer, obj.backColorId);
                if (obj.UseMapBuffer)
                {
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.backColorVertices.Length * 3 * sizeof(float)), IntPtr.Zero, BufferUsageHint.StreamDraw);
                }
                else
                {
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.backColorVertices.Length * 3 * sizeof(float)),
                        obj.backColorVertices, BufferUsageHint.StreamDraw);
                }

            }

            uint[] indices = CreateIndices(width * height);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, obj.iboId);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);


            if (obj.UseBackColor)
            {
                if (obj.shader != null)
                {
                    //TODO: add this as optionable?
                    obj.shader.PassArgumentMethods.Add((a) =>
                    {
                        //Back Color Draw
                        //Color Buffer
                        GL.BindBuffer(BufferTarget.ArrayBuffer, obj.backColorId);
                        GL.ColorPointer(3, ColorPointerType.Float, sizeof(float)*3, 0);
                        if (obj.UseMapBuffer)
                        {
                            unsafe
                            {
                                IntPtr VideoMemoryIntPtr = GL.MapBuffer(BufferTarget.ArrayBuffer, BufferAccess.WriteOnly);
                                //ErrorCode e = GL.GetError();
                                fixed (Vector3* SystemMemory = &obj.backColorVertices[0])
                                {
                                    Vector3* vertexArray = (Vector3*)VideoMemoryIntPtr.ToPointer();
                                    //if (vertexArray != null && vertexArray != (void*)0)
                                    {
                                        for (int i = 0; i < obj.backColorVertices.Length; i++)
                                        {
                                            vertexArray[i].X = SystemMemory[i].X;
                                            vertexArray[i].Y = SystemMemory[i].Y;
                                            vertexArray[i].Z = SystemMemory[i].Z;
                                        }
                                    }
                                }
                            }
                            GL.UnmapBuffer(BufferTarget.ArrayBuffer);
                        }
                        else if (obj.ModelChanged)
                        {
                            GL.BufferSubData(BufferTarget.ArrayBuffer,IntPtr.Zero,
                                (IntPtr)(obj.backColorVertices.Length * 3 * sizeof(float)), obj.backColorVertices);
                        }

                        int backColorID = GL.GetAttribLocation(a.ShaderProgramID, "backColor");
                        GL.EnableVertexAttribArray(backColorID);
                        GL.VertexAttribPointer(backColorID, 3, VertexAttribPointerType.Float,
                            false, 0, 0);
                    });
                }
            }

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }
        public void DrawDynamicObject(RLColor transparentColor, RLDynamicObject obj, Vector2 modelPosition, float modelAngle, Vector2 cameraPosition, bool drawRotated = false, float offsetAngle = 0, Vector2 offsetPosition = default(Vector2), Vector3 scale = default(Vector3))
        {
            
            if (obj.shader!=null&&obj.shader.UseShader)
                GL.UseProgram(obj.shader.ShaderProgramID);
            else
                GL.UseProgram(0);
            float charU = (float)CharWidth * uRatio;
            float charV = (float)CharHeight * vRatio;


            int width = (drawRotated ? obj.rotatedCells.GetLength(0) : obj.renderedCells.GetLength(0));
            int height = (drawRotated ? obj.rotatedCells.GetLength(1) : obj.renderedCells.GetLength(1));

            float halfRealWidth = width / 2 * this.CharWidth;
            float halfRealHeight = height / 2 * this.CharHeight;
            GL.PushMatrix();



            //remember commands are executed from last one!
            GL.Translate(new Vector3(-cameraPosition.X * this.CharWidth, -cameraPosition.Y * this.CharHeight, 0));
/*            if (offsetPosition != Vector2.Zero)
            {
                GL.Translate(new Vector3((float)(offsetPosition.X * this.CharWidth * Math.Sin(modelAngle * (Math.PI / 180.000))), (float)(offsetPosition.Y * this.CharHeight * Math.Cos(modelAngle * (Math.PI / 180.000))), 0));
            }*/
            GL.Translate(new Vector3(modelPosition.X * this.CharWidth, modelPosition.Y * this.CharHeight, 0));
            //TODO: pass vector from 0-1 defining arouch which point rotation should occur
            GL.Translate(new Vector3(halfRealWidth, halfRealHeight, 0));

            if (!drawRotated)
                GL.Rotate(modelAngle, 0, 0, 1);
            GL.Translate(new Vector3(-halfRealWidth, -halfRealHeight, 0));
            if (offsetAngle != 0)
            {
                GL.Translate(new Vector3(halfRealWidth, halfRealHeight, 0));

                if (!drawRotated)
                    GL.Rotate(offsetAngle, 0, 0, 1);

                GL.Translate(new Vector3(-halfRealWidth, -halfRealHeight, 0));
            }
            if (scale != default(Vector3))
            {
                GL.Translate(new Vector3(halfRealWidth, (height * scale.Y / 4 * this.CharHeight) + (height / 4 * this.CharHeight), 0));
                GL.Scale(scale);
                GL.Translate(new Vector3(-halfRealWidth, -halfRealHeight, 0));
            }

            if (obj.ModelChanged)
            {
                for (int iy = 0; iy < height; iy++)
                {
                    for (int ix = 0; ix < width; ix++)
                    {

                        RLCell cell = (drawRotated ? obj.rotatedCells[ix, iy] : obj.renderedCells[ix, iy]);
                        int i = (ix + (iy * width)) * 4;
                        int character = cell.character;


                        float charX = (float)(character % charsPerRow) * charU;
                        float charY = (float)(character / charsPerRow) * charV;

                        obj.texVertices[i].X = charX + charU;
                        obj.texVertices[i].Y = charY;
                        obj.texVertices[i + 1].X = charX;
                        obj.texVertices[i + 1].Y = charY;
                        obj.texVertices[i + 2].X = charX;
                        obj.texVertices[i + 2].Y = charY + charV;
                        obj.texVertices[i + 3].X = charX + charU;
                        obj.texVertices[i + 3].Y = charY + charV;
                        if (obj.UseBackColor)
                        {
                            Vector3 backColor = (character < 1)
                                ? transparentColor.ToVector3()
                                : cell.backColor.ToVector3();

                            obj.backColorVertices[i] = backColor;
                            obj.backColorVertices[i + 1] = backColor;
                            obj.backColorVertices[i + 2] = backColor;
                            obj.backColorVertices[i + 3] = backColor;
                        }
                    }
                }
            }
            if (obj.ModelChanged || obj.FrontColorChanged)
            {
                for (int iy = 0; iy < height; iy++)
                {
                    for (int ix = 0; ix < width; ix++)
                    {

                        RLCell cell = (drawRotated ? obj.rotatedCells[ix, iy] : obj.renderedCells[ix, iy]);
                        int i = (ix + (iy * width)) * 4;
                        int character = cell.character;

                        Vector3 color = (character < 1) ? transparentColor.ToVector3() : cell.color.ToVector3();
                        obj.colorVertices[i] = color;
                        obj.colorVertices[i + 1] = color;
                        obj.colorVertices[i + 2] = color;
                        obj.colorVertices[i + 3] = color;
                    }
                }
            }
            //Vertex Buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, obj.vboId);
            GL.VertexPointer(2, VertexPointerType.Float, 2 * sizeof(float), 0);
            //Index Buffer
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, obj.iboId);


            if(obj.shader!=null)
                obj.shader.PassArgumentsToShader();

            //Fore Color / Texture Draw
            //Texture Coord Buffer
            GL.Enable(EnableCap.Texture2D);
            GL.EnableClientState(ArrayCap.TextureCoordArray);

            GL.BindBuffer(BufferTarget.ArrayBuffer, obj.tcboId);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 2 * sizeof(float), 0);
            if (obj.UseMapBuffer)
            {

                unsafe
                {
                    IntPtr VideoMemoryIntPtr = GL.MapBuffer(BufferTarget.ArrayBuffer, BufferAccess.WriteOnly);
                    //ErrorCode e = GL.GetError();
                    fixed (Vector2* SystemMemory = &obj.texVertices[0])
                    {
                        Vector2* vertexArray = (Vector2*)VideoMemoryIntPtr.ToPointer();
                        //if (vertexArray != null && vertexArray != (void*)0)
                        {
                            for (int i = 0; i < obj.texVertices.Length; i++)
                            {
                                vertexArray[i].X = SystemMemory[i].X;
                                vertexArray[i].Y = SystemMemory[i].Y;
                            }
                        }
                    }
                }
                GL.UnmapBuffer(BufferTarget.ArrayBuffer);
            }
            else if (obj.ModelChanged)
            {
                GL.BufferSubData(BufferTarget.ArrayBuffer, IntPtr.Zero, (IntPtr)(obj.texVertices.Length * 2 * sizeof(float)),
                    obj.texVertices);
            }
            //Color Buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, obj.foreColorId);
            GL.ColorPointer(3, ColorPointerType.Float, 3 * sizeof(float), 0);
            if (obj.UseMapBuffer)
            {

                unsafe
                {
                    IntPtr VideoMemoryIntPtr = GL.MapBuffer(BufferTarget.ArrayBuffer, BufferAccess.WriteOnly);
                    //ErrorCode e = GL.GetError();
                    fixed (Vector3* SystemMemory = &obj.colorVertices[0])
                    {
                        Vector3* vertexArray = (Vector3*)VideoMemoryIntPtr.ToPointer();
                        //if (vertexArray != null && vertexArray != (void*)0)
                        {
                            for (int i = 0; i < obj.colorVertices.Length; i++)
                            {
                                vertexArray[i].X = SystemMemory[i].X;
                                vertexArray[i].Y = SystemMemory[i].Y;
                                vertexArray[i].Z = SystemMemory[i].Z;
                            }
                        }
                    }
                }
                GL.UnmapBuffer(BufferTarget.ArrayBuffer);
            }
            else if (obj.ModelChanged || obj.FrontColorChanged)
            {

                GL.BufferSubData(BufferTarget.ArrayBuffer, IntPtr.Zero, (IntPtr)(obj.colorVertices.Length * 3 * sizeof(float)),
                    obj.colorVertices);
            }
            //Draw
            GL.DrawElements(PrimitiveType.Triangles, width * height * 6, DrawElementsType.UnsignedInt, 0);

            GL.DisableClientState(ArrayCap.TextureCoordArray);

            GL.PopMatrix();


            GL.UseProgram(0);
            obj.ModelChanged = false;
        }

        private Vector2[] CreateDynamicVertices(int width, int height, int charWidth, int charHeight)
        {
            Vector2[] vertices = new Vector2[width * height * 4];
            for (int iy = 0; iy < height; iy++)
            {
                for (int ix = 0; ix < width; ix++)
                {
                    int i = (ix + (iy * width)) * 4;

                    //1 -- 0
                    //2 -- 3

                    int x = ix * charWidth;
                    int y = iy * charHeight;

                    vertices[i].X = x + charWidth;
                    vertices[i].Y = y;

                    vertices[i + 1].X = x;
                    vertices[i + 1].Y = y;

                    vertices[i + 2].X = x;
                    vertices[i + 2].Y = y + charHeight;

                    vertices[i + 3].X = x + charWidth;
                    vertices[i + 3].Y = y + charHeight;
                }
            }
            return vertices;
        }
        private void LoadFontTexture(string filename, bool alpha)
        {
            if (guiObject.guiTexId != 0) GL.DeleteTexture(guiObject.guiTexId);
            if(alpha)
                guiObject.guiTexAlphaId = GL.GenTexture();
            else
                guiObject.guiTexId = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, alpha ? guiObject.guiTexAlphaId : guiObject.guiTexId);
            //Create Texture
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
            Bitmap bmp = new Bitmap(filename);
            BitmapData bmpData = bmp.LockBits(
                new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            charsPerRow = bmp.Width / CharWidth;
            uRatio = 1f / (float)bmp.Width;
            vRatio = 1f / (float)bmp.Height;

            //Set Alpha
            IntPtr ptr = bmpData.Scan0;
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            int r = rgbValues[0];
            int b = rgbValues[1];
            int g = rgbValues[2];
            for (int i = 0; i < rgbValues.Length; i += 4)
            {
                if (rgbValues[i] == r && rgbValues[i + 1] == b && rgbValues[i + 2] == g)
                    rgbValues[i + 3] = 0;
            }

            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);


            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);
            bmp.UnlockBits(bmpData);

        }
    }
}
