﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using GL = OpenTK.Graphics.ES20.GL;
using ProgramParameter = OpenTK.Graphics.ES20.ProgramParameter;
using ShaderParameter = OpenTK.Graphics.ES20.ShaderParameter;
using ShaderType = OpenTK.Graphics.ES20.ShaderType;

namespace RLNET
{
    public class RLShader
    {
        private string vertexShaderText;
        private string fragmentShaderText;
        public int ShaderProgramID;
        //TODO: add proper add/remove method
        public List<Action<RLShader>> PassArgumentMethods;
        //TODO: add list of textures?
        public int TextureID;
        public bool UseShader;
        public string VertexShaderText { get { return vertexShaderText; }
            set
            {
                vertexShaderText = value;
               // CreateShaderProgram(vertexShaderText, fragmentShaderText);
            }
        }
        public string FragmentShaderText
        {
            get { return fragmentShaderText; }
            set
            {
                fragmentShaderText = value;
                //CreateShaderProgram(vertexShaderText, fragmentShaderText);
            }
        }

        public RLShader(string vertText, string fragText, string additionalTexturePath="")
        {
            this.VertexShaderText = vertText;
            this.FragmentShaderText = fragText;
            //TODO: disable old one if exists
            ShaderProgramID = CreateShaderProgram(vertexShaderText, fragmentShaderText);
            TextureID = -1;
            if (!string.IsNullOrEmpty(additionalTexturePath))
            {
                LoadTexture2D(additionalTexturePath, ref TextureID);
            }
            PassArgumentMethods = new List<Action<RLShader>>();
            UseShader = true;
        }

        public void PassArgumentsToShader()
        {
            if (TextureID >= 0)
            {
                BindTexture(ref TextureID, TextureUnit.Texture1, ShaderProgramID, "iChannel0");
            }
            for (int i = 0; i < PassArgumentMethods.Count; i++)
            {
                PassArgumentMethods[i](this);
            }
        }
        protected static int CreateShader(ShaderType type, string vertexShader)
        {
            string vertShaderSrc = vertexShader;
            int shaderObj = GL.CreateShader(type);
            GL.ShaderSource(shaderObj, vertShaderSrc);
            GL.CompileShader(shaderObj);
            int success;
            GL.GetShader(shaderObj, ShaderParameter.CompileStatus, out success);
            if (success == 0)
            {
                Console.WriteLine("### Error compiling shader " + " ###");
                Console.WriteLine(GL.GetShaderInfoLog(shaderObj));
            }
            return shaderObj;
        }

        protected static int CreateShaderProgram(string vertShaderText, string fragShaderText)
        {
            int ShaderProgramID = GL.CreateProgram();

            int vertShader = CreateShader(ShaderType.VertexShader, vertShaderText);
            GL.AttachShader(ShaderProgramID, vertShader);
            int fragShader = CreateShader(ShaderType.FragmentShader, fragShaderText);
            GL.AttachShader(ShaderProgramID, fragShader);

            // GL.TransformFeedbackVaryings(shaderPass1, 1, new string[] { "gl_Position" }, TransformFeedbackMode.InterleavedAttribs);
            LinkShaderProgram(ShaderProgramID);
            GL.ValidateProgram(ShaderProgramID);
            
            return ShaderProgramID;
        }

        protected static void LinkShaderProgram(int program)
        {
            GL.LinkProgram(program);
            int success;
            GL.GetProgram(program, ProgramParameter.LinkStatus, out success);
            if (success == 0)
            {
                Console.WriteLine("### Error linking shader program ###");
                Console.WriteLine(GL.GetProgramInfoLog(program));
            }
        }

        private void BindTexture(ref int textureId, TextureUnit textureUnit, int shaderProgram, string UniformName)
        {
            //http://www.opentk.com/node/2559
            OpenTK.Graphics.OpenGL.GL.ActiveTexture(textureUnit);
            OpenTK.Graphics.OpenGL.GL.BindTexture(TextureTarget.Texture2D, textureId);
            OpenTK.Graphics.OpenGL.GL.Uniform1(OpenTK.Graphics.OpenGL.GL.GetUniformLocation(shaderProgram, UniformName), textureUnit - TextureUnit.Texture0);
        }
        private void LoadTexture2D(string filename, ref int texID)
        {
            texID = OpenTK.Graphics.OpenGL.GL.GenTexture();
            OpenTK.Graphics.OpenGL.GL.BindTexture(TextureTarget.Texture2D, texID);
            //Create Texture
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.Linear);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            OpenTK.Graphics.OpenGL.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
            Bitmap bmp = new Bitmap(filename);
            BitmapData bmpData = bmp.LockBits(
                new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);


            OpenTK.Graphics.OpenGL.GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);
            bmp.UnlockBits(bmpData);

        }
    }
}
