﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLNET
{
    public class RLCollision
    {

        /// <summary>
        /// 0 means no collsion
        /// </summary>
        public int[,] CollisionFlagsMap;

        public int MapWidth;
        public int MapHeight;

        public RLCollision(int width, int height)
        {
            MapWidth = width;
            MapHeight = height;
            CollisionFlagsMap = new int[width,height];
        }

        public bool IsSet<T>(T flags, int x, int y) where T : struct
        {
            int flagsValue = (int) (object) flags;
            int flagValue = CollisionFlagsMap[x, y];

            return (flagsValue & flagValue) == flagValue;
        }

        public bool HaveAnythingSet(int x, int y)
        {
            return CollisionFlagsMap[x, y] > 0;
        }
        public void Set<T>(ref T flags, int x, int y) where T : struct
        {
            int flagsValue = (int) (object) flags;
            int flagValue = CollisionFlagsMap[x, y];

            flags = (T) (object) (flagsValue | flagValue);
        }

        public void Unset<T>(ref T flags, int x, int y) where T : struct
        {
            int flagsValue = (int) (object) flags;
            int flagValue = CollisionFlagsMap[x, y];

            flags = (T) (object) (flagsValue & (~flagValue));
        }
    }
}
