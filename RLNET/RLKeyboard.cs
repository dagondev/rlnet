﻿#region license
/* 
 * Released under the MIT License (MIT)
 * Copyright (c) 2014 Travis M. Clark
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
#endregion

using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Input;

namespace RLNET
{
    public class RLKeyboard
    {
        private bool numLock;
        private bool capsLock;
        private bool scrollLock;
        private bool alt;
        private bool shift;
        private bool control;
        private GameWindow gameWindow;
        private static Array keyArray;
        private HashSet<Key> pressedKeys;
        private List<RLKeyEvent> currentPolledKeyEvents;
        internal RLKeyboard(GameWindow gameWindow)
        {
            this.gameWindow = gameWindow;
            this.gameWindow.Keyboard.KeyDown += Keyboard_KeyDown;
            this.gameWindow.Keyboard.KeyDown += Keyboard_KeyUp;
            keyArray = Enum.GetValues(typeof(OpenTK.Input.Key));
            pressedKeys=new HashSet<Key>();
            currentPolledKeyEvents = new List<RLKeyEvent>();
        }

        private void Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            alt = e.Alt;
            shift = e.Shift;
            control = e.Control;
        }

        void Keyboard_KeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.NumLock) numLock = !numLock;
            else if (e.Key == Key.CapsLock) capsLock = !capsLock;
            else if (e.Key == Key.ScrollLock) scrollLock = !scrollLock;
            alt = e.Alt;
            shift = e.Shift;
            control = e.Control;
        }

        public List<RLKeyEvent> PollKeyEvents()
        {
            return currentPolledKeyEvents;
        }
        internal void PollKeyEvents(double delta)
        {
            currentPolledKeyEvents = new List<RLKeyEvent>();
            KeyboardDevice keyboard = gameWindow.Keyboard;
            foreach (var v in keyArray)
            {
                RLKeyEvent keyEvent = new RLKeyEvent();
                OpenTK.Input.Key key = (Key) v;
                if (keyboard[key])
                {
                    if (!pressedKeys.Contains(key))
                        pressedKeys.Add(key);

                    keyEvent.Key = (RLKey) key;
                    keyEvent.Pressed = true;
                    keyEvent.Alt = alt;
                    keyEvent.CapsLock = capsLock;
                    keyEvent.Control = control;
                    keyEvent.Shift = shift;
                    keyEvent.ScrollLock = scrollLock;
                    keyEvent.NumLock = numLock;
                    currentPolledKeyEvents.Add(keyEvent);
                    continue;
                }
                if (!pressedKeys.Contains(key))
                    continue;
                pressedKeys.Remove(key);
                keyEvent.Key = (RLKey) key;
                keyEvent.Pressed = false;
                keyEvent.Alt = alt;
                keyEvent.CapsLock = capsLock;
                keyEvent.Control = control;
                keyEvent.Shift = shift;
                keyEvent.ScrollLock = scrollLock;
                keyEvent.NumLock = numLock;
                currentPolledKeyEvents.Add(keyEvent);
            }
        } 

    }
}
